/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "rostlab/blosum62.h"

const signed char rostlab::blosum62[27][27] = {
//         *   A   B   C   D   E   F   G   H   I   J   K   L   M   N   o   P   Q   R   S   T   u   V   W   X   Y   Z
/* * */ {  1, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4,  0, -4, -4, -4, -4, -4,  0, -4, -4, -4, -4, -4, },
/* A */ { -4,  4, -2,  0, -2, -1, -2,  0, -2, -1, -1, -1, -1, -1, -2,  0, -1, -1, -1,  1,  0,  0,  0, -3, -1, -2, -1, },
/* B */ { -4, -2,  4, -3,  4,  1, -3, -1,  0, -3, -3,  0, -4, -3,  4,  0, -2,  0, -1,  0, -1,  0, -3, -4, -1, -3,  0, },
/* C */ { -4,  0, -3,  9, -3, -4, -2, -3, -3, -1, -1, -3, -1, -1, -3,  0, -3, -3, -3, -1, -1,  0, -1, -2, -1, -2, -3, },
/* D */ { -4, -2,  4, -3,  6,  2, -3, -1, -1, -3, -3, -1, -4, -3,  1,  0, -1,  0, -2,  0, -1,  0, -3, -4, -1, -3,  1, },
/* E */ { -4, -1,  1, -4,  2,  5, -3, -2,  0, -3, -3,  1, -3, -2,  0,  0, -1,  2,  0,  0, -1,  0, -2, -3, -1, -2,  4, },
/* F */ { -4, -2, -3, -2, -3, -3,  6, -3, -1,  0,  0, -3,  0,  0, -3,  0, -4, -3, -3, -2, -2,  0, -1,  1, -1,  3, -3, },
/* G */ { -4,  0, -1, -3, -1, -2, -3,  6, -2, -4, -4, -2, -4, -3,  0,  0, -2, -2, -2,  0, -2,  0, -3, -2, -1, -3, -2, },
/* H */ { -4, -2,  0, -3, -1,  0, -1, -2,  8, -3, -3, -1, -3, -2,  1,  0, -2,  0,  0, -1, -2,  0, -3, -2, -1,  2,  0, },
/* I */ { -4, -1, -3, -1, -3, -3,  0, -4, -3,  4,  3, -3,  2,  1, -3,  0, -3, -3, -3, -2, -1,  0,  3, -3, -1, -1, -3, },
/* J */ { -4, -1, -3, -1, -3, -3,  0, -4, -3,  3,  3, -3,  3,  2, -3,  0, -3, -2, -2, -2, -1,  0,  2, -2, -1, -1, -3, },
/* K */ { -4, -1,  0, -3, -1,  1, -3, -2, -1, -3, -3,  5, -2, -1,  0,  0, -1,  1,  2,  0, -1,  0, -2, -3, -1, -2,  1, },
/* L */ { -4, -1, -4, -1, -4, -3,  0, -4, -3,  2,  3, -2,  4,  2, -3,  0, -3, -2, -2, -2, -1,  0,  1, -2, -1, -1, -3, },
/* M */ { -4, -1, -3, -1, -3, -2,  0, -3, -2,  1,  2, -1,  2,  5, -2,  0, -2,  0, -1, -1, -1,  0,  1, -1, -1, -1, -1, },
/* N */ { -4, -2,  4, -3,  1,  0, -3,  0,  1, -3, -3,  0, -3, -2,  6,  0, -2,  0,  0,  1,  0,  0, -3, -4, -1, -2,  0, },
/* o */ {  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, },
/* P */ { -4, -1, -2, -3, -1, -1, -4, -2, -2, -3, -3, -1, -3, -2, -2,  0,  7, -1, -2, -1, -1,  0, -2, -4, -1, -3, -1, },
/* Q */ { -4, -1,  0, -3,  0,  2, -3, -2,  0, -3, -2,  1, -2,  0,  0,  0, -1,  5,  1,  0, -1,  0, -2, -2, -1, -1,  4, },
/* R */ { -4, -1, -1, -3, -2,  0, -3, -2,  0, -3, -2,  2, -2, -1,  0,  0, -2,  1,  5, -1, -1,  0, -3, -3, -1, -2,  0, },
/* S */ { -4,  1,  0, -1,  0,  0, -2,  0, -1, -2, -2,  0, -2, -1,  1,  0, -1,  0, -1,  4,  1,  0, -2, -3, -1, -2,  0, },
/* T */ { -4,  0, -1, -1, -1, -1, -2, -2, -2, -1, -1, -1, -1, -1,  0,  0, -1, -1, -1,  1,  5,  0,  0, -2, -1, -2, -1, },
/* u */ {  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, },
/* V */ { -4,  0, -3, -1, -3, -2, -1, -3, -3,  3,  2, -2,  1,  1, -3,  0, -2, -2, -3, -2,  0,  0,  4, -3, -1, -1, -2, },
/* W */ { -4, -3, -4, -2, -4, -3,  1, -2, -2, -3, -2, -3, -2, -1, -4,  0, -4, -2, -3, -3, -2,  0, -3, 11, -1,  2, -2, },
/* X */ { -4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1, -1, -1, -1,  0, -1, -1, -1, -1, -1, },
/* Y */ { -4, -2, -3, -2, -3, -2,  3, -3,  2, -1, -1, -2, -1, -1, -2,  0, -3, -1, -2, -2, -2,  0, -1,  2, -1,  7, -2, },
/* Z */ { -4, -1,  0, -3,  1,  4, -3, -2,  0, -3, -3,  1, -3, -1,  0,  0, -1,  4,  0,  0, -1,  0, -2, -2, -1, -2,  4, }
};
