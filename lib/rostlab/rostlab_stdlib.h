/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_STDLIB
#define ROSTLAB_STDLIB 1

#include <errno.h>
#include <stdexcept>
#include <stdarg.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <vector>

namespace rostlab {

typedef std::vector<std::string>
                  argvec_type;

inline argvec_type
                  mkargvec( const char* __path, ... );
inline bool       file_exists( const std::string& __path );
inline int        system( const char* __path, ... );
inline int        system( const std::vector<std::string>& __args );
inline std::string     tolower( std::string __str );


inline bool       file_exists( const std::string& __path )
{
  struct stat buf;
  if( stat( __path.c_str(), &buf ) ) return false;
  return true;
}


inline argvec_type
                  mkargvec( const char* __path, ... )
{
  std::vector<std::string>  argvec;
  argvec.push_back( __path );

  va_list listPointer;
  va_start( listPointer, __path );

  char* arg;
  while( ( arg = va_arg( listPointer, char* ) ) != NULL )
  {
    argvec.push_back( arg );
  }
  va_end( listPointer );

  return argvec;
}


inline int        system( const char* __path, ... )
{
  std::vector<std::string>  argvec;
  argvec.push_back( __path );

  va_list listPointer;
  va_start( listPointer, __path );

  char* arg;
  while( ( arg = va_arg( listPointer, char* ) ) != NULL )
  {
    argvec.push_back( arg );
  }
  va_end( listPointer );

  return system( argvec );
}

inline int        system( const std::vector<std::string>& __args )
{
  pid_t pid = fork();
  if( pid == -1 ) throw runtime_error( strerror( errno ) );
  if( !pid )
  {
    char* argv[ __args.size()+1 ];
    for( size_t i = 0; i < __args.size(); ++i ) argv[i] = const_cast<char*>( __args[i].c_str() );
    argv[__args.size()] = NULL;

    if( execvp( argv[0], argv ) ) throw runtime_error( strerror( errno ) );
    exit(0);
  }
  int status;
  if( !waitpid( pid, &status, 0 ) ) throw runtime_error( strerror( errno ) );
  if( !WIFEXITED(status) ) throw runtime_error( "child exited abnormally" );
  return status;
}


inline std::string     tolower( std::string __str )
{
  for( std::string::iterator s_i = __str.begin(); s_i != __str.end(); ++s_i ) *s_i = ::tolower( *s_i );
  return __str;
}

} // namespace rostlab

#endif // ROSTLAB_STDLIB
// vim:et:ts=4:ai:
