/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_MAPAA2INT
#define ROSTLAB_MAPAA2INT 1

#include <ctype.h>
#include <rostlab/rostlab_stdexcept.h>
#include <sstream>

namespace rostlab {

inline char       int2AAchar (int __aanum)
{
        switch(__aanum){
            case 0:
                return 'A';
            case 1:
                return 'R';
            case 2:
                return 'N';
            case 3:
                return 'D';
            case 4:
                return 'C';
            case 5:
                return 'Q';
            case 6:
                return 'E';
            case 7:
                return 'G';
            case 8:
                return 'H';
            case 9:
                return 'I';
            case 10: 
                return 'L';
            case 11: 
                return 'K';
            case 12: 
                return 'M';
            case 13: 
                return 'F';
            case 14: 
                return 'P';
            case 15: 
                return 'S';
            case 16: 
                return 'T';
            case 17: 
                return 'W';
            case 18: 
                return 'Y';
            case 19: 
                return 'V';
            default:
                std::ostringstream buf; buf << "invalid amino acid numeric code '" << __aanum << "'";
                throw runtime_error( buf.str() );
        }   
}

inline int        AAchar2int (char __aachar)
{
        switch(toupper(__aachar)){
            case 'A':
                return 0;
            case 'R':
                return 1;
            case 'N':
                return 2;
            case 'D':
                return 3;
            case 'C':
                return 4;
            case 'Q':
                return 5;
            case 'E':
                return 6;
            case 'G':
                return 7;
            case 'H':
                return 8;
            case 'I':
                return 9;
            case 'L':
                return 10; 
            case 'K':
                return 11; 
            case 'M':
                return 12; 
            case 'F':
                return 13; 
            case 'P':
                return 14; 
            case 'S':
                return 15; 
            case 'T':
                return 16; 
            case 'W':
                return 17; 
            case 'Y':
                return 18; 
            case 'V':
                return 19; 
            default:
                std::ostringstream buf; buf << "invalid amino acid '" << __aachar << "'";
                throw runtime_error( buf.str() );
        }   
}
}
#endif

// vim:et:ts=2:
