/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_STDEXCEPT
#define ROSTLAB_STDEXCEPT

#include <execinfo.h>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace rostlab {

class error_backtracer
{
  private:
    std::string        _backtrace;
  public:
                    error_backtracer()
    {
      const int     bufsize = 64;
      void*         buffer[bufsize];
      char **strings;

      _backtrace.reserve(4096);

      size_t nptrs = ::backtrace(buffer, bufsize);
      strings = backtrace_symbols(buffer, nptrs);

      if (strings == NULL) { perror("backtrace_symbols"); exit(EXIT_FAILURE); }

      for( size_t j = 0; j < nptrs; ++j )
      {
        _backtrace += std::string( strings[j] ) + "\n";
        if( _backtrace.capacity() == _backtrace.size() ) _backtrace.reserve( _backtrace.size() * 2 );
      }

      free(strings);
    }
    virtual         ~error_backtracer() throw() {}

    virtual const std::string&
                    backtrace() const { return _backtrace; }
};

class exception : public error_backtracer, public std::exception
{
  public:
                    exception() : error_backtracer(), std::exception(){}
};

class runtime_error : public error_backtracer, public std::runtime_error
{
  public:
                    runtime_error( const std::string& __msg ) : error_backtracer(), std::runtime_error( __msg + "\nBacktrace:\n" + backtrace() ){}
};

} // namespace rostlab

#endif // ROSTLAB_STDEXCEPT
// vim:et:ts=4:ai:syntax=cpp:
