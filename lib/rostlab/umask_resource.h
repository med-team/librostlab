/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_UMASK_RESOURCE
#define ROSTLAB_UMASK_RESOURCE 1

#include <sys/types.h>
#include <sys/stat.h>

namespace rostlab {

class umask_resource
{
  private:
    mode_t        _old_mode;
    // this is a resource - disable copy contructor and copy assignment
                  umask_resource( const umask_resource& ){};
    umask_resource&
                  operator=(const umask_resource&){return *this;};
  public:
                  umask_resource( mode_t __new_mode ) : _old_mode( umask( __new_mode ) ) {};
    virtual       ~umask_resource(){ umask( _old_mode ); }
};

};

#endif // ROSTLAB_UMASK_RESOURCE
// vim:et:ts=2:ai:
