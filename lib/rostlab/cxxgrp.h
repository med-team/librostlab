/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_CXXGRP
#define ROSTLAB_CXXGRP 1

#include <boost/format.hpp>
#include <errno.h>
#include <grp.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <vector>

#include "rostlab/rostlab_stdexcept.h"

namespace bo = boost;

namespace rostlab {

struct cxx_group
{
  std::string          gr_name;       /* group name */
  std::string          gr_passwd;     /* group password */
  gid_t           gr_gid;        /* group ID */
  std::vector<std::string>  gr_mem;        /* group members */

  inline          cxx_group(){};
  inline          cxx_group( const std::string& __gr_name );
  inline          cxx_group( const std::string& __gr_name, const std::string& __gr_passwd, gid_t __gr_gid, const std::vector<std::string>& __gr_mem ) :
                    gr_name(__gr_name), gr_passwd(__gr_passwd), gr_gid(__gr_gid), gr_mem(__gr_mem) {};
  inline          cxx_group( const std::string& __gr_name, const std::string& __gr_passwd, gid_t __gr_gid, char** __gr_mem ) :
                    gr_name(__gr_name), gr_passwd(__gr_passwd), gr_gid(__gr_gid) { while(*__gr_mem) gr_mem.push_back( *__gr_mem++ ); };
};

class gid_not_found_error : public runtime_error { public: gid_not_found_error( const std::string& what ) : runtime_error(what) {} };
class gname_not_found_error : public runtime_error { public: gname_not_found_error( const std::string& what ) : runtime_error(what) {} };

// namespace functions
inline std::string     getgrgid_r( gid_t __gid );
inline std::string     getgrgid_r( gid_t __gid, cxx_group& __group );
inline gid_t      getgrnam_r( const std::string& __gname );
inline gid_t      getgrnam_r( const std::string& __gname, struct cxx_group& __group );


inline            cxx_group::cxx_group( const std::string& __gr_name )
{
  rostlab::getgrnam_r( __gr_name, *this );
};

inline std::string     getgrgid_r( gid_t __gid, cxx_group& __group )
{
  struct group gbuf;
  struct group *gbufp;

  size_t buflen = sysconf( _SC_GETGR_R_SIZE_MAX );
  char* buf = static_cast<char *>( malloc(buflen) ); if(!buf) throw runtime_error( bo::str( bo::format("failed to allocate %d bytes") % buflen ) );

  try{
    int _errno;
    do {
      _errno = ::getgrgid_r( __gid, &gbuf, buf, buflen, &gbufp );
      if( _errno == ERANGE ) // too small buffer
      {
        buflen *= 2;
        buf = static_cast<char *>( realloc( buf, buflen ) ); if(!buf) throw runtime_error( bo::str( bo::format("failed to allocate %d bytes") % buflen ) );
      }
      else if( _errno ) throw runtime_error( bo::str( bo::format("failed to getgrgid_r '%d': %s") % __gid % strerror( errno ) ) );
    } while( _errno );

    if( gbufp == NULL ) throw gid_not_found_error( bo::str( bo::format("gid '%d' not found") % __gid ) );

    __group = cxx_group( gbuf.gr_name, gbuf.gr_passwd, gbuf.gr_gid, gbuf.gr_mem );
  }
  catch(...)
  {
    free(buf);
    throw;
  }
  free(buf); buf = NULL;

  return __group.gr_name;
}

inline std::string     getgrgid_r( gid_t __gid )
{
  struct cxx_group        grp;
  return getgrgid_r( __gid, grp );
}


inline gid_t    getgrnam_r( const std::string& __gname, struct cxx_group& __group )
{
  struct group gbuf;
  struct group *gbufp;

  size_t buflen = sysconf( _SC_GETGR_R_SIZE_MAX );
  char* buf = static_cast<char *>( malloc(buflen) ); if(!buf) throw runtime_error( bo::str( bo::format("failed to allocate %d bytes") % buflen ) );

  try{
    int _errno;
    do {
      _errno = ::getgrnam_r( __gname.c_str(), &gbuf, buf, buflen, &gbufp );
      if( _errno == ERANGE ) // too small buffer
      {
        buflen *= 2;
        buf = static_cast<char *>( realloc( buf, buflen ) ); if(!buf) throw runtime_error( bo::str( bo::format("failed to allocate %d bytes") % buflen ) );
      }
      else if( _errno ) throw runtime_error( bo::str( bo::format("failed to getgrnam_r '%s': %s") % __gname % strerror( errno ) ) );
    } while( _errno );

    if( gbufp == NULL ) throw gname_not_found_error( bo::str( bo::format("gname '%s' not found") % __gname ) );

    __group = cxx_group( gbuf.gr_name, gbuf.gr_passwd, gbuf.gr_gid, gbuf.gr_mem );
  }
  catch(...)
  {
    free(buf);
    throw;
  }
  free(buf); buf = NULL;

  return gbuf.gr_gid;
}


inline gid_t    getgrnam_r( const std::string& __gname )
{
  struct cxx_group        grp;
  return getgrnam_r( __gname, grp );
}

}; // namespace rostlab

#endif // ROSTLAB_CXXGRP
// vim:et:ai:ts=2:
